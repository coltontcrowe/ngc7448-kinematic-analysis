# NGC7448 Kinematic Analysis

A lab report from college where I did an analysis of the NGC7448 Galaxy.

The final version of the report can be seen [here](https://gitlab.com/coltontcrowe/ngc7448-kinematic-analysis/-/blob/master/Ngc7448KinematicAnalysis.pdf).