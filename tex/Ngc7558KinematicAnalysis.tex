\documentclass[11pt]{article}
\usepackage{placeins,amsmath}
\usepackage{graphicx}
\setlength\parindent{0pt}
\usepackage[top=1.in, bottom=1.in, left=0.75in, right=0.75in]{geometry}
\title{Kinematic Evaluation of NGC7448}
\author{Colton Crowe}

\begin{document}
\maketitle

\section{Abstract}
I aim to evaluate several aspects of the Kinematics of galaxy NGC7448.  The analysis focuses on the $H_\alpha$ spectral line and aims to derive several important quantities from this.  This includes finding the relative velocity and distance of the galaxy which leads to a calculation of the kinematic mass.


\section{Introduction}
This report aims to give several key features of the kinematics of the galaxy NGC7448.  The measurements were done at the Hartung Boothroyd Observatory at Cornell University.  Using M57 (the Ring Nebula), we aim to fit a curve matching wavelength to position on the spectrograph.  We will then deal with any error present from a tilt in the spectrograph and determine how the wavelength changes in the $H_\alpha$ band of the spectrograph.  This can give the velocity of the rotation of the galaxy which will give us the kinematic mass.

\section{Observations}

To start, we took a bias in order to get the read noise for the CCD.  To avoid the issue of cosmic rays, the median will be used instead of the mean.

\begin{equation}
\text{RN} = 499 \pm 3 DN
\end{equation}


From here, two spectra were taken$-$one of NGC7448 and one of M57 for comparison.

\begin{figure}[h]
\caption{Spectrum of NGC7448}
\begin{center}
	\includegraphics[scale=0.4]{images/ngc7448spec}
\end{center}
\end{figure}%
\FloatBarrier

\begin{figure}[h]
\caption{Spectrum of M57 (The Ring Nebula)}
\begin{center}
	\includegraphics[scale=0.4]{images/m57spec}
\end{center}
\end{figure}%
\FloatBarrier

The CCD was set up in the following way for the observations:

\begin{table}[!htdp]
\caption{Journal of Observations}
\begin{center}
	\begin{tabular}{| c | c | c | c | c | c | }
	\hline
	Image & Time (UTC) & Air Mass & Exposure (s) & Temperature (C) & Readout Speed (kHz) \\ \hline
	Bias & 01:55:00AM & n/a & 0 &-60 & 33 \\ \hline
	M57 & 01:48:00AM & 1.07 & 300 & -60 & 33 \\ \hline
	NGC7448 & 02:15:00AM & 1.26 & 900 & -60 & 33 \\
	\hline
	\end{tabular}
\end{center}
\label{journobs}
\end{table}%
\FloatBarrier

Additionally, the Spectrograph was set at $600gr/mm 135mm, 200um slit, 12 d$ and the data was taken on 2014-09-16.

\section{Analysis}

The program JTV is first used to locate the positions of various lines in the spectrograph and fit wavelengths to them.  This is done with the data from M57 as the redshift given from it is negligible.  All data will be taken at y position $y_{\text{ref}}=293\pm0.5$ and the wavelengths given to the program are from http://coursewiki.astro.cornell.edu/Astro4410/LineLists.  Also, an error of $\pm0.5$ is implied on the x position values as these are only accurate to the nearest pixel. The analysis gives these positions of well-known lines.


\begin{table}[!htdp]
\caption{Positions of Spectral Lines}
\begin{center}
	\begin{tabular}{| c | c | c | }
	\hline
	Line Name & Wavelength & X Position (pixels) \\ \hline
	$[S$ $II]$ (right)& $6730.8$ & $1603.01 \pm 0.08$ \\ \hline
	$[S$ $II]$ (left)&  $6716.4$ & $1594.49 \pm 0.07$ \\ \hline
	$H_{\alpha} $& $6562.8$ & $1499.46 \pm 0.02$ \\ \hline
	$[N$ $II]$ (right) & $6583.5$ & $1512.30 \pm 0.01$ \\
	\hline
	\end{tabular}
\end{center}
\label{xlambda}
\end{table}%
\FloatBarrier

From this data, JTV decides on the fit $\lambda = 4838.1227 + 0.71106103x + 0.00029291041x^2$.  From here, we need to consider the tilt in the spectrograph as a source of error.  To do this, I focus on the $[S \, III]$ line at around 6312.1 and check how the fitted wavelength varies with the y position.

\begin{table}[!htdp]
\caption{Y Center vs Wave Center}
\begin{center}
	\begin{tabular}{| c | c | }
	\hline
	Y Center & Wave Center\\ \hline
	$503$ & $6311.65 \pm 0.19$ \\  \hline
	$488$ & $6311.94 \pm 0.19$ \\ \hline
	$467$ & $6311.89 \pm 0.18$ \\ \hline
	$449$ & $6312.16 \pm 0.19$ \\ \hline
	$429$ & $6312.43 \pm 0.19$ \\ \hline
	$411$ & $6312.72 \pm 0.19$ \\ \hline
	$392$ & $6312.9 \pm 0.2$ \\ \hline
	$365$ & $6313.21 \pm 0.19$ \\ \hline
	$345$ & $6313.6 \pm 0.2$ \\ \hline
	$331$ & $6313.9 \pm 0.2$ \\ \hline
	$308$ & $6314.5 \pm 0.2$ \\ \hline
	$287$ & $6314.6 \pm 0.2$ \\ \hline
	$268$ & $6315.02 \pm 0.19$ \\  \hline
	$247$ & $6315.5 \pm 0.2$ \\  \hline
	$228$ & $6315.82 \pm 0.19$ \\  \hline
	$208$ & $6316.40 \pm 0.18$ \\  \hline
	$187$ & $6316.84 \pm 0.18$ \\  \hline
	$164$ & $6317.53 \pm 0.18$ \\  \hline
	$143$ & $6318.01 \pm 0.19$ \\  \hline
	$122$ & $6318.43 \pm 0.19$ \\ \hline
	$95$ & $6319.34 \pm 0.19$ \\ \hline
	$76$ & $6319.54 \pm 0.19$ \\
	\hline
	\end{tabular}
\end{center}
\label{yvwave}
\end{table}%
\FloatBarrier

This is then plotted with a quadratic curve to fit:
\begin{figure}[h]
\caption{Spectrum of NGC7448}
\begin{center}
	\includegraphics[scale=0.5]{images/lambdacorrection}
\end{center}
\end{figure}%
\FloatBarrier

The curve of fit, giving the correction $\Delta \lambda$ in terms of the vertical displacement is\\
$\Delta \lambda =2.34967498769\cdot 10^{-5}\Delta y^2-0.0326374593916\Delta y+6322.10796826$\\



From NGC7448, we look at the tilt in the $H_{\alpha}$ line as y varied.
\begin{table}[!htdp]
\caption{$H_\alpha$ Wavelength vs Y Center}
\begin{center}
	\begin{tabular}{| c | c | }
	\hline
	Y Center & Wave Center \\ \hline
	$394$ & $6605.1 \pm 0.9$ \\ \hline
	$381$ & $6602 \pm 4$ \\ \hline
	$376$ & $6607.4 \pm 1.6$ \\ \hline
	$372$ & $6605 \pm 2$ \\ \hline
	$364$ & $6607.1 \pm 0.9$ \\ \hline
	$359$ & $6607.9 \pm 0.6$ \\ \hline
	$352$ & $6608.29 \pm 0.18$ \\ \hline
	$344$ & $6607.68 \pm 0.12$ \\ \hline
	$337$ & $6608.1 \pm 0.2$ \\ \hline
	$333$ & $6608.3 \pm 0.3$ \\ \hline
	$327$ & $6609.4 \pm 0.3$ \\ \hline
	$323$ & $6610.3 \pm 0.3$ \\ \hline
	$317$ & $6611.7 \pm 0.3$ \\ \hline
	$310$ & $6612.76 \pm 0.19$ \\ \hline
	$304$ & $6613.3 \pm 0.3$ \\ \hline
	$298$ & $6614.1 \pm 0.3$ \\ \hline
	$293$ & $6614.73 \pm 0.16$ \\ \hline
	$284$ & $6614.83 \pm 0.18$ \\ \hline
	$281$ & $6615.44 \pm 0.19$ \\ \hline
	$274$ & $6615.91 \pm 0.16$ \\ \hline
	$268$ & $6615.7 \pm 0.3$ \\ \hline
	$262$ & $6615.3 \pm 0.6$ \\ \hline
	$256$ & $6611.9 \pm 0.8$ \\ \hline
	$246$ & $6615.2 \pm 0.8$ \\ \hline
	$243$ & $6615.0 \pm 0.7$ \\ \hline

	\hline
	\end{tabular}
\end{center}
\label{galaxy halpha}
\end{table}%
\FloatBarrier


From the table of the NGC7448 $H_\alpha$ line, we get the plot:

\begin{figure}[h]
\caption{Plot of Uncorrected Wavelength vs Y Position}
\begin{center}
	\includegraphics[scale=0.5]{images/badwave}
\end{center}
\end{figure}%
\FloatBarrier

We then add the corrections from the previous plot to get:

\begin{figure}[h]
\caption{Corrected Wavelength vs Y Position}
\begin{center}
	\includegraphics[scale=0.7]{images/goodwave}
\end{center}
\end{figure}%
\FloatBarrier

From this plot, we attempt to determine the dynamical center of the galaxy.  To do this, we notice that the plot begins to become flat around the points $(274\pm 0.5, \, 6619.32 \pm 0.16)$ and $(344\pm 0.5, \, 6611.39 \pm 0.12)$.  Note that the choice of these two points is somewhat arbitrary and could result in the introduction of systematic error.  We will take the mean of the two wavelengths to get the dynamical center to be located where the wavelength is $6615.36 \pm 0.10 \AA$.  This is very close to the point $(317 \pm 0.5, \, 6615.3 \pm 0.3)$, so we will treat this as our dynamical center.

With the dynamical center, we can determine the redshift of the galaxy by the calculation:

\begin{equation}
Z = \frac{\lambda - \lambda_{\text{rest}}}{\lambda_{\text{rest}}}
\end{equation}

We will calculate this with $\lambda_{\text{rest}} = 6562.8 \pm 0.05 \AA$ which, again, comes from\\ http://coursewiki.astro.cornell.edu/Astro4410/LineLists.  Note that the error was not present, so the 0.05 was used as a reasonable estimate.  This yields the result,

\begin{equation}
Z = 0.00800 \pm 0.00005
\end{equation}

Honestly, I am incredibly skeptical of such an error; however, provided that the systematic errors had been controlled, this could be quite reasonable.

With the Redshift, it is simple enough to calculate velocity as it is simply $v=Zc$ where $c=299792.458 km/s$.  This gives:
\begin{equation}
v= 2398 \pm 15 km/s
\end{equation}

This, however, does not correct for the motion of our own galaxy, to determine the correction for our galaxy, I use IDL as described at http://coursewiki.astro.cornell.edu/Astro4410/RadialVelocities.  The values for Right Ascension and Declination were found at http://ned.ipac.caltech.edu.  This gives me a correction of $257.11718898 \pm 0.000000005km/s$.  Thus, it turns out that

\begin{equation}
v= 2655 \pm 15 km / s
\end{equation}
with respect to the local group barycenter.

And from here, we get distance by using the equation:

\begin{equation}
D=\frac{v}{H_0}
\end{equation}

where $H_0$ is Hubble's Constant.  While a precise value for the constant is still debated, we will use $73.8 \pm 2.4 km / s / Mpc$ as is given in Riess' paper .  With this, we find the distance from us to the galaxy is approximately:

\begin{equation}
D = 36.0 \pm 3.2 Mpc
\end{equation}


With this, we turn our attention to converting the pixels to actual distances.  To do this, we note that when running the telescope at the given specs there is a conversion of $0.78 \pm 0.005$ arc seconds / pixel from http://coursewiki.astro.cornell.edu/Astro4410/HboInstruments.  Converting this to radians, we get $(3.78 \pm 0.02) \cdot 10^{-6} \text{radians/pixel}$.  Using simple trigonometry and a small angle approximation, we find that:

\begin{equation}
\Delta y =D \tan \theta \approx D \theta
\end{equation}

Thus, using our results, we find that

\begin{equation}
\Delta y = 0.13536 \pm  0.00015 kpc / \text{pixel}
\end{equation}

Now, with this, we can convert the earlier differences in pixels into actual distance from the center of the galaxy.\\ \\

If we repeat the earlier processes on various points in the galaxy, we can get an idea of how these quantities vary with the radius of the galaxy.  This culminates in a calculation of the mass based on these earlier quantities.  The mass is found by equating the centrifugal acceleration with the gravitational force present in the galaxy.  Note that in the case of a symmetric body, the gravitational force is approximately as though the mass contained in the given radius is concentrated at the center.  This is only approximate as we are working with a spiral in a plane rather than a sphere.  Thus, we have:

\begin{equation}
\frac{mv^2}{r}=\frac{GMm}{r^2}
\end{equation}

By solving this, we find

\begin{equation}
M=\frac{rv^2}{G}
\end{equation}

For G, we use $G=(6.674315 \pm 0.000092) \cdot 10^{-11}m^3/kg/s^2$ as is given at\\
 $\text{http://asd.gsfc.nasa.gov/Stephen.Merkowitz/G/Big\_G.html}$.  If we convert this into the unit's that we are using,

\begin{equation}
G=(4.30235521 \pm 0.00000008)\cdot 10^{-11} kpc/(M\odot)^{-1} (km/s)^2
\end{equation}

We can use this on all of our data points,but we must keep in mind that the $v$ in this equation is the velocity relative to the center of the galaxy.  Also, the velocities must be corrected by the same correction given earlier.   Taking these into account, we can calculate our results:

\begin{table}[!htdp]
\caption{Analysis of Data Points}
\begin{center}
	\begin{tabular}{| c | c | c | c | c | }
	\hline
	Radius (kpc) & Wavelength ($\AA$) & Redshift & Velocity (km/s) & Mass (M $\odot$) \\ \hline
	 $10.42 \pm 0.10$ & $6608.8 \pm 0.9 $& $0.0070163\pm9e-7$       &$2360.6\pm0.3$ &$(2.1\pm0.2)10^{11}$ \\ \hline
	  $8.66 \pm 0.10$& $6606 \pm 4 $         & $0.006564\pm4e-6$         & $2224.9\pm1.3$& $(3.7\pm0.2)10^{11}$\\ \hline
	  $7.99 \pm 0.10$& $6611.1 \pm 1.6$    & $0.0073631\pm1.8e-6$   & $2464.5\pm0.5$& $(6.7\pm0.8)10^{10}$\\ \hline
	  $7.44 \pm 0.10$& $6608.6 \pm 1.8$    & $0.0069814\pm1.9e-6$   & $2350.1\pm0.6$& $(1.6\pm0.1)10^{11}$\\ \hline
	  $6.36 \pm 0.10$& $6610.8 \pm 0.9$    & $0.0073208\pm1.0e-6$   & $2451.8\pm0.3$& $(6.1\pm0.6)10{10}$\\ \hline
	  $5.69 \pm 0.10$& $6611.6 \pm 0.6$    & $0.0074397\pm7e-7$      & $2487.5\pm0.2$& $(3.7\pm0.5)10^{10}$\\ \hline
	  $4.73 \pm 0.10$& $6612.02 \pm 0.18$& $0.0074999\pm2e-7$      & $2505.52\pm0.07$& $(2.5\pm0.4)10^{10}$\\ \hline
	  $3.65 \pm 0.10$& $6611.39 \pm 0.12$& $0.00740388\pm1.6e-7$& $2476.75\pm0.05$& $(2.7\pm0.3)10^{10}$\\ \hline
	  $2.71 \pm 0.10$& $6611.8 \pm 0.2$    & $0.00746482\pm2e-7$    & $2495.01\pm0.07$& $(1.6\pm0.2)10^{10}$\\ \hline
	  $2.17 \pm 0.10$& $6611.9 \pm 0.3$    & $0.0074888\pm4e-7$      & $2502.21\pm0.11$& $(1.2\pm0.2)10^{10}$\\ \hline
	  $1.35 \pm 0.10$& $6613.0 \pm 0.3$    & $0.0076518\pm4e-7$      & $2551.07\pm0.11$& $(3.3\pm0.7)10^9$\\ \hline
	  $0.81 \pm 0.10$& $6614.0 \pm 0.3$    & $0.0077989\pm4e-7$      & $2595.19\pm0.11$& $(7\pm3)10^8$\\ \hline
	  $0.      \pm 0.$& $6615.3 \pm 0.3$    & $0.0079981\pm4e-7$      & $2654.90\pm0.11$& $(0 \pm 0)$\\ \hline
	  $-0.95 \pm 0.10$& $6616.35 \pm 0.19$&$0.0081598 \pm3e-7$     & $2703.36\pm0.08$& $(5\pm2)10^8$\\ \hline
	  $-1.75 \pm 0.10$& $6616.9 \pm 0.3$    & $0.0082380\pm4e-7$      & $2726.81\pm0.11$& $(2.1\pm0.6)10^9$\\ \hline
	  $-2.57 \pm 0.10$& $6617.6 \pm 0.3$    & $0.0083556\pm4e-7$      & $2762.06\pm0.12$& $(7\pm1)10^9$\\ \hline
	  $-3.25 \pm 0.10$& $6618.24 \pm 0.16$& $0.0084478\pm2e-7$     & $2789.70\pm0.07$& $(1.4\pm0.2)10^{10}$\\ \hline
	  $-4.47 \pm 0.10$& $6618.29 \pm 0.18$& $0.0084558\pm2e-7$     & $2792.09\pm0.07$& $(2.0\pm0.3)10^{10}$\\ \hline
	  $-4.87 \pm 0.10$& $6618.89 \pm 0.19$& $0.0085461\pm3e-7$     & $2819.19\pm0.08$& $(3.1\pm0.4)10^{10}$\\ \hline
	  $-5.82 \pm 0.10$& $6619.32 \pm 0.16$& $0.00861155\pm2e-7$   & $2838.80\pm0.07$& $(4.6\pm0.5)10^{10}$\\ \hline
	  $-6.63 \pm 0.10$& $6619.0 \pm 0.3$    & $0.0085709\pm4e-7$      & $2826.61\pm0.12$& $(4.5\pm0.6)10^{10}$\\ \hline
	  $-7.44 \pm 0.10$& $6618.6 \pm 0.6$    & $0.00850865\pm8e-7$    & $2807.9\pm0.2$& $(4.0\pm0.6)10^{10}$\\ \hline
	  $-8.26 \pm 0.10$& $6618.4 \pm 0.8$    & $0.00847815\pm1.0e-6$ & $2798.8\pm0.3$& $(4.0\pm0.6)10^{10}$\\ \hline
	  $-9.61 \pm 0.10$& $6618.4 \pm 0.8$    & $0.00847195\pm1.0e-6$& $2796.9\pm0.3$& $(4.5\pm0.7)10^{10}$\\ \hline
	  $-10.01 \pm 0.10$& $6618.2 \pm 0.7$ & $0.00844115\pm9e-7$    & $2787.7\pm0.3$& $(4.1\pm0.7)10^{10}$\\
	\hline
	\end{tabular}
\end{center}
\label{analysis chart}
\end{table}%
\FloatBarrier

When we make a plot of the enclosed mass as we vary the radius, we get:

\begin{figure}[h]
\caption{Enclosed Mass vs Radius}
\begin{center}
	\includegraphics[scale=0.7]{images/massplot}
\end{center}
\end{figure}%
\FloatBarrier

\section{Discussion}
While the fits generally worked quite well in most areas, there is some concern as to whether or not systematic error could have caused problems.  This error could have been introduced when a center for the galaxy was chosen as the choice was somewhat arbitrary despite being based on the symmetry of the galaxy.  It would be incredibly useful if a more concrete method for determining the center could be used.  Also, while areas in the middle and toward the bottom of the galaxy were very well behaved, the upper areas of the galaxy posed some problems that persisted throughout the graphs.  This may have been due to cosmic rays and further data in this area would be helpful.  In any case, this data has larger error bars and seems somewhat erratic, so the distance in the other direction will be used instead.  Indeed, the mass begins at $0 M\odot$ in the center but quickly grows as the radius increases.  This, however, does taper off and ends at about $(2.1\pm0.2)10^{11} (M \odot)$ which appears to be a reasonable mass for the galaxy.


\section{Conclusion}
Using a few techniques in Spectroscopy, we have managed to find a relative distance of $36.0 \pm 3.2 Mpc$ and relative velocity of $2655 \pm 15 km / s$ between us and the galaxy NGC7448.  Additionally, by looking at the difference in spin, we have determined that the mass of the galaxy is about $(2.1\pm0.2)10^{11} (M \odot)$.  To go from here, it would be nice to compare this with the luminous velocity in order to see what can be ascertained.

\section{References}
 Barry, Don. "HBO Instruments." HboInstruments. Cornell, 30 Aug. 2013. Web. 29 Sept. 2014.\\ \\
Barry, Don. "Line Lists." LineLists. Cornell, n.d. Web. 29 Sept. 2014.\\ \\
Barry, Don. "Some Notes on Radial Velocities." RadialVelocities. Cornell University, 12 June 2012. Web. 29 Sept. 2014.
"HBO Instruments." HboInstruments. N.p., n.d. Web. 29 Sept. 2014.\\ \\
Merkowitz, Stephen. "University of Washington Big G Measurement." University of Washington Big G Measurement. N.p., 23 Dec. 2002. Web. 29 Sept. 2014.\\ <http://asd.gsfc.nasa.gov/Stephen.Merkowitz/G/Big\_G.html>.\\ \\
Riess, Adam G., Lucas Macri, Stefano Casertano, Hubert Lampeit, Henry C. Ferguson, Alexei V. Filippenko, Saurabh W. Jha, Weidong Li, Ryan Chornock, and Jeffrey M. Silverman. "�A 3\% SOLUTION: DETERMINATION OF THE HUBBLE CONSTANT WITH THE HUBBLE SPACE TELESCOPE AND WIDE FIELD CAMERA 3� (ApJ, 2011, 730, 119)." The Astrophysical Journal 732.2 (2011): 129. Web.\\ \\
"Your NED Search Results." Nasa/Ipac Extragalctic Database. NASA, n.d. Web. 29 Sept. 2014.









\end{document}
