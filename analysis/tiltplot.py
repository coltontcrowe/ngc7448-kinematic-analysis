'''a graph of the tilt of a spectral line due to systematic error'''

from numpy import array,vstack,linalg,ones,polyfit,poly1d
from matplotlib import pyplot
class tiltplot():
    def __init__(self):
        ycent=[503,488,467,449,429,411,392,365,345,331,308,287,268,247,
               228,208,187,164,143,122,95,76]
        self.ycent=array(ycent)

        wcent=[6311.65,6311.94,6311.89,6312.16,6312.43,6312.72,6312.86,
               6313.21,6313.56,6313.87,6314.46,6314.57,6315.02,6315.52,
               6315.82,6316.40,6316.84,6317.53,6318.01,6318.43,6319.34,6319.54]
        self.wcent=array(wcent)

        wcenterr=[0.19,0.19,0.18,0.19,0.19,0.19,0.21,0.19,0.22,0.24,0.24,
                  0.21,0.19,0.20,0.19,0.18,0.18,0.18,0.19,0.19,0.19,0.19]
        self.wcenterr=array(wcenterr)

        A = vstack([self.ycent, ones(len(ycent))]).T

        self.m, self.c = linalg.lstsq(A, self.wcent)[0]

        self.z = polyfit(self.ycent, self.wcent, 2)
        self.zfunc=poly1d(self.z)
        

    def res(self,x):
        return -self.zfunc(x+293)+self.zfunc(293)

    def printout(self):
        print 'wcent={0}ycent+{1}'.format(self.m,self.c)
        print 'wcent={0}ycent^2+{1}ycent+{2}'.format(self.z[0],self.z[1],self.z[2])

    def plotting(self):
        pyplot.title('Correction for Wavelengths due to Displacement from Reference')
        pyplot.errorbar(self.ycent-293,-self.wcent+self.zfunc(293), yerr=self.wcenterr, marker='o',linestyle='',label='Original data', markersize=5)
        #pyplot.plot(ycent, m*ycent + c, 'r', label='Fitted line')
        pyplot.plot(self.ycent-293, -self.zfunc(self.ycent)+self.zfunc(293), 'g', label='Fitted Polynomial')
        pyplot.legend(loc=2)
        pyplot.rc('text', usetex=True)
        pyplot.xlabel('Displacement from Reference Point (pixels)')
        pyplot.ylabel(r'Correction in Reference Wavelength ($\AA$)')
        pyplot.show()
    
    

if __name__=='__main__':
    a=tiltplot()
    a.plotting()
