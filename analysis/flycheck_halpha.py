'''a graph of the tilt of a spectral line due to systematic error'''

from numpy import array,vstack,linalg,ones,polyfit,poly1d
from matplotlib import pyplot
class halpha():
    def __init__(self):
        ycent=[394,381,376,372,364,359,352,344,337,333,327,323,317,310,304,298,293,284,281,274,268,262,
               256,246,243]
               
        self.ycent=array(ycent)

        wcent=[6605.06,6602.10,6607.35,6604.85,6607.09,6607.88,6608.29,6607.68,6608.1,6608.27,6609.36,6610.34,6611.67,6612.76,
               6613.3,6614.1,6614.73,6614.83,6615.44,6615.91,6615.68,6615.31,
               6615.15,6615.18,6615.0]
        self.wcent=array(wcent)

        wcenterr=[0.85,4.31,1.63,1.80,0.87,0.58,0.18,0.12,0.2,0.3,0.3,0.3,0.3,0.19,0.3,0.3,0.16,0.18,0.19,0.16,
                  0.3,0.6,0.8,0.8,0.7]
        self.wcenterr=array(wcenterr)

        A = vstack([self.ycent, ones(len(ycent))]).T

        self.m, self.c = linalg.lstsq(A, self.wcent)[0]

        self.z = polyfit(self.ycent, self.wcent, 3)
        self.zfunc=poly1d(self.z)
        

    def res(self,x):
        return -self.zfunc(x+293)+self.zfunc(293)

    def printout(self):
        print 'wcent={0}ycent+{1}'.format(self.m,self.c)
        print 'wcent={0}ycent^3+{1}ycent^2+{2}ycent+{3}'.format(self.z[0],self.z[1],self.z[2],self.z[3])

    def plotting(self):
        pyplot.title('Wavelength (Uncorrected) vs Y Position')
        pyplot.errorbar(self.ycent,self.wcent, yerr=self.wcenterr, marker='o',label='Original data', markersize=5)
        #pyplot.plot(ycent, m*ycent + c, 'r', label='Fitted line')
        #pyplot.plot(self.ycent, 4.94110965639e-05*self.ycent**3-0.0464765511875*self.ycent**2+14.4009257067*self.ycent+5142.41748814, 'g', label='Fitted Polynomial')
        pyplot.legend()
        pyplot.rc('text', usetex=True)
        pyplot.xlabel('Y Position (pixels)')
        pyplot.ylabel(r'Uncorrected Wavelength ($\AA$)')
        pyplot.show()
    
    

if __name__=='__main__':
    a=tiltplot()
    a.plotting()
