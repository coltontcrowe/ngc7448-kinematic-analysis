'''plot corrected halpha'''

from halpha import halpha
from tiltplot import tiltplot
from matplotlib import pyplot
from numpy import array

class corhalpha(object):
    def __init__(self):
        self.t = tiltplot()
        self.h = halpha()
        self.yvals=self.h.ycent
        self.wvals=self.h.wcent + self.t.res(self.h.ycent)
        errs1=2*self.h.ycent*0.5*2.35e-05 - 0.033*0.5
        errs2=self.h.wcenterr
        self.werrs=(errs1**2+errs2**2)**0.5
        self.disp=(self.yvals-317)*0.13536
        self.disperr=[]
        for i in self.yvals:
            self.disperr.append((i-317)*0.13536*((0.5*2**0.5/(i-317+0.0001))**2+(0.00015/0.13536)**2)**0.5)
        self.disperr=array(self.disperr)
        self.redshift=(self.wvals-6562.8)/6562.8
        self.redshifterr=self.redshift*(((self.werrs**2+0.05**2)**0.5/(self.werrs - 6562.8))**2+(0.05/6562.8)**2)**0.5
        self.v=self.redshift*299792.458+257.11718898
        self.verr=self.redshifterr*299792.458
        self.mass=abs(self.disp)*(abs(self.v)-2655)**2 / (4.30235521e-06)
        self.masserr=self.mass*((self.disperr/(self.disp+1e-8))**2+((self.verr**2+15**2)**0.5/(abs(self.v)-2655+1e-8))**2 * 2)**0.5

    def plotting(self):
        pyplot.rc('text', usetex=True)
        pyplot.title('Wavelength vs Y Position')
        pyplot.errorbar(self.yvals,self.wvals,yerr=self.werrs,marker='o',markersize=3)
        pyplot.xlabel('Y Position (pixels)')
        pyplot.ylabel('Wavelength ($\AA$)')
        pyplot.show()

    def massplot(self):
        pyplot.title('Enclosed Mass vs Radius')
        pyplot.rc('text', usetex=True)
        pyplot.errorbar(self.disp,self.mass,yerr=self.masserr,xerr=self.disperr,marker='o',markersize=5)
        pyplot.xlabel('Radius (kpc)')
        pyplot.ylabel('Enclosed Mass ($M\odot$)')
        pyplot.show()

    def printing(self):
        print "wavelengths are " + str(self.wvals)
        print "errors are " + str(self.werrs)
